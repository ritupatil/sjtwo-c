#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "message.h"
#include "message_processor.h"

bool message_processor(void) {
  bool symbol_found = false;
  message_s message;
  memset(&message, 0, sizeof(message));

  for (size_t message_count = 0; message_count < max_messages_to_process; message_count++) {
    if (!message__read(&message)) {
      break;
    } else {
      if (process_msg(&message, message_count)) {
        symbol_found = true;
      }
    }
  }

  return symbol_found;
}