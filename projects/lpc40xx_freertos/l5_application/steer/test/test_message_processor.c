#include "unity.h"

#include "Mockmessage.h"
#include "message_processor.h"

void test_process_3_messages(void) {
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_with_dollar_sign(void) {
  message_s msg;
  msg.data[0] = '$';
  msg.data[1] = 'C';
  msg.data[2] = '2';
  msg.data[3] = '4';
  msg.data[4] = '3';
  msg.data[5] = '\0';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(&msg, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&msg);

  TEST_ASSERT_TRUE(message_processor());
}

void test_process_message_with_hash_sign(void) {
  message_s msg;
  msg.data[0] = '#';
  msg.data[1] = 'R';
  msg.data[2] = 'B';
  msg.data[3] = '\0';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(&msg, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&msg);

  TEST_ASSERT_TRUE(message_processor());
}

void test_process_messages_without_any_dollar_or_hash_sign(void) {
  message_s msg;
  msg.data[0] = 'x';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(&msg, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&msg);

  TEST_ASSERT_FALSE(message_processor());
}

void test_process_msg_after_dollar_sign() {
  message_s msg;
  TEST_ASSERT_FALSE(process_msg(&msg, 1));
  TEST_ASSERT_FALSE(process_msg(&msg, 0));

  msg.data[0] = '$';
  msg.data[1] = 'C';
  msg.data[2] = '2';
  msg.data[3] = '4';
  msg.data[4] = '3';
  msg.data[5] = '\0';
  TEST_ASSERT_TRUE(process_msg(&msg, 2));

  msg.data[1] = 'D';
  msg.data[2] = '2';
  msg.data[3] = '4';
  msg.data[4] = '3';
  msg.data[5] = '\0';
  // Should return false ase this is not the expected string
  TEST_ASSERT_FALSE(process_msg(&msg, 2));
}

void test_process_msg_after_hash_sign() {
  message_s msg;
  TEST_ASSERT_FALSE(process_msg(&msg, 1));
  TEST_ASSERT_FALSE(process_msg(&msg, 0));

  msg.data[0] = '#';
  msg.data[1] = 'R';
  msg.data[2] = 'B';
  msg.data[3] = '\0';
  TEST_ASSERT_TRUE(process_msg(&msg, 2));

  msg.data[1] = 'A';
  msg.data[2] = 'G';
  msg.data[3] = '\0';

  TEST_ASSERT_FALSE(process_msg(&msg, 2));
}

void test_process_msg_after_random_sign() {
  message_s msg;
  msg.data[0] = 'x';
  TEST_ASSERT_FALSE(process_msg(&msg, 2));
  TEST_ASSERT_FALSE(process_msg(&msg, 1));
  TEST_ASSERT_FALSE(process_msg(&msg, 0));
}