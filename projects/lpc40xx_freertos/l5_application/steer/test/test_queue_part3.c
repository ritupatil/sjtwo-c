#include "unity.h"

#include "queue_part3.h"

void test_empty(void) {
  queue_s queue;
  queue__init_part3(&queue, 0, 0);
  uint8_t popped_value = 0;
  TEST_ASSERT_FALSE(queue__pop_part3(&queue, &popped_value));
}

void test_AA_push_pop(void) {
  queue_s queue;
  static uint8_t hex_value[2];
  queue__init_part3(&queue, hex_value, 2);
  TEST_ASSERT_TRUE(queue__push_part3(&queue, 0x1A));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop_part3(&queue, &popped_value));
  TEST_ASSERT_EQUAL(0x1A, popped_value);
}
void test_comprehensive_part3(void) {
  static uint8_t max_queue_size[128];
  queue_s queue;
  queue__init_part3(&queue, max_queue_size, sizeof(max_queue_size));
  for (size_t item = 0; item < sizeof(max_queue_size); item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_part3(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count_part3(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue__push_part3(&queue, 123));
  TEST_ASSERT_EQUAL(sizeof(max_queue_size), queue__get_item_count_part3(&queue));

  // Pull and verify the FIFO order
  for (size_t item = 0; item < sizeof(max_queue_size); item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop_part3(&queue, &popped_value));

    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Test wrap-around case
  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push_part3(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop_part3(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count_part3(&queue));
  TEST_ASSERT_FALSE(queue__pop_part3(&queue, &popped_value));
}
