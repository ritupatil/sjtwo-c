#include "message.h"
#include <stdbool.h>
#include <string.h>
bool message_processor(void);

const static size_t max_messages_to_process = 3;

static inline bool process_msg(message_s *msg, size_t count) {
  if (count != max_messages_to_process - 1) {
    return false;
  }

  switch (msg->data[0]) {
  case '$':
    if (strcmp(&msg->data[1], "C243") == 0) {
      return true;
    } else {
      return false;
    }
  case '#':
    if (strcmp(&msg->data[1], "RB") == 0) {
      return true;
    } else {
      return false;
    }
  default:
    return false;
  }
  return false;
}