

#include "steer_processor.h"
#include "steering.h"

const int threshold = 50;
void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {
  if ((right_sensor_cm >= threshold) && (left_sensor_cm < threshold))
    steer_right();
  else if ((right_sensor_cm < threshold) && (left_sensor_cm >= threshold))
    steer_left();
  else if ((right_sensor_cm < threshold) && (left_sensor_cm < threshold)) {
    if (right_sensor_cm > left_sensor_cm)
      steer_right();
    else
      steer_left();
  }
}
