#include "queue_part3.h"
#include "string.h"

void queue__init_part3(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  queue->static_memory_for_queue = static_memory_for_queue;
  queue->static_memory_size_in_bytes = static_memory_size_in_bytes;
  queue->front = -1;
  queue->rear = -1;
}

/// @returns false if the queue is full
bool queue__push_part3(queue_s *queue, uint8_t push_value) {
  if (queue->rear == ((uint8_t)(queue->static_memory_size_in_bytes)) - 1) {
    return false;
  }

  queue->rear++;
  if (queue->rear == 0)
    queue->front++;
  queue->static_memory_for_queue[queue->rear] = push_value;

  return true;
}

/// @returns false if the queue was empty
/// Write the popped value to the user provided pointer pop_value_ptr
bool queue__pop_part3(queue_s *queue, uint8_t *pop_value_ptr) {
  if ((queue->front == -1) && (queue->rear == -1))
    return false;

  *pop_value_ptr = queue->static_memory_for_queue[queue->front];
  if (queue->front == queue->rear) {
    queue->front = -1;
    queue->rear = -1;
  } else
    queue->front++;

  return true;
}

size_t queue__get_item_count_part3(const queue_s *queue) {
  if ((queue->front == -1) && (queue->rear == -1))
    return 0;
  else
    return ((queue->rear) - (queue->front) + 1);
}