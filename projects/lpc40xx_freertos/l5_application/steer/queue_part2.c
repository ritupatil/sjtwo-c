#include "queue_lab2.h"

void queue__init(queue_s *queue) {
  queue->front = -1;
  queue->rear = -1;
  queue->capacity = 100;
}

/// @returns false if the queue is full
bool queue__push(queue_s *queue, uint8_t push_value) {
  if (queue->rear == (queue->capacity) - 1)
    return false;
  queue->rear++;
  queue->queue_memory[queue->rear] = push_value;
  if (queue->rear == 0)
    queue->front++;
  return true;
}

/// @returns false if the queue was empty
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  if ((queue->front == -1) && (queue->rear == -1))
    return false;

  *pop_value = queue->queue_memory[queue->front];
  if (queue->front == queue->rear) {
    queue->front = -1;
    queue->rear = -1;
  } else
    queue->front++;
  return true;
}

size_t queue__get_item_count(const queue_s *queue) {
  if ((queue->front == -1) && (queue->rear == -1))
    return 0;
  else
    return ((queue->rear) - (queue->front) + 1);
}